# Admin Website using CoreUI library.

This project was development by [Dat Tran](https://github.com/).

## Available Scripts

In the project directory, you can run:

#### Step 1: `npm i`

#### Step 2: `npm start`

Runs the app in the development mode.\
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
