export const REQUEST_SUCCESS = 200;
export const ERROR_401 = 401;
export const ERROR_403 = 403;
export const ERROR_404 = 404;
export const ERROR_500 = 500;