import React from 'react';
import {CDataTable} from "@coreui/react";

const StripedTable = (props) => {

    return (
        <CDataTable
            size={props.size}
            tableFilter={props.tableFilter}
            items={props.items}
            fields={props.fields}
            itemsPerPageSelect={{label: 'Selected', values: [10, 25, 50, 100]}}
            itemsPerPage={props.pagination.itemsPerPage}
            pagination={props.pagination}
            hover={props.hover}
            sorter={props.sorter}
            scopedSlots={props.scopedSlots}
            onPageChange={props.onPageChange}
        
        />
    );
};

export default StripedTable;
