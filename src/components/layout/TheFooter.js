import {CFooter} from '@coreui/react';
import React from "react";

const TheFooter = () => {
    return (
        <CFooter fixed={false}>
            <div>
                <a href="https://payme.vn/web/home" target="_blank" rel="noopener noreferrer">PayME Vietnam</a>
                <span className="ml-1">&copy; 2020 PayME.</span>
            </div>
            <div className="mfs-auto">
                <span className="mr-1">Powered by</span>
                <a href="https://payme.vn/web/home" target="_blank" rel="noopener noreferrer">PayME</a>
            </div>
        </CFooter>
    );
}

export default TheFooter

