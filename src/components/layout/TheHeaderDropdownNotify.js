import React from 'react';

import {
    CBadge,
    CDropdown,
    CDropdownItem,
    CDropdownMenu,
    CDropdownToggle
} from '@coreui/react';

import {CIcon} from '@coreui/icons-react';
import {freeSet} from '@coreui/icons'

const TheHeaderDropdownNotify = () => {
    const itemsCount = 5;
    return (
        <CDropdown
            inNav
            className="c-header-nav-item mx-2"
        >
            <CDropdownToggle className="c-header-nav-link" caret={false}>
                <CIcon content={freeSet.cilBell} size={'lg'}/>
                <CBadge shape="pill" color="danger">{itemsCount}</CBadge>
            </CDropdownToggle>
            <CDropdownMenu placement="bottom-end" className="pt-0">
                <CDropdownItem
                    header
                    tag="div"
                    className="text-center"
                    color="light">
                    <strong>You have {itemsCount} notifications</strong>
                </CDropdownItem>
                <CDropdownItem>
                    <CIcon content={freeSet.cilUserFollow} className="mr-2 text-success"/>
                    New user registered
                </CDropdownItem>
                <CDropdownItem>
                    <CIcon content={freeSet.cilUserUnfollow} className="mr-2 text-danger"/>
                    User deleted
                </CDropdownItem>
                <CDropdownItem>
                    <CIcon content={freeSet.cilChartPie} className="mr-2 text-info"/>
                    Sales report is ready
                </CDropdownItem>
                <CDropdownItem>
                    <CIcon content={freeSet.cilBasket} className="mr-2 text-primary"/>
                    New client</CDropdownItem>
                <CDropdownItem>
                    <CIcon content={freeSet.cilSpeedometer} className="mr-2 text-warning"/>
                    Server overloaded
                </CDropdownItem>
            </CDropdownMenu>
        </CDropdown>
    )
}

export default TheHeaderDropdownNotify;
