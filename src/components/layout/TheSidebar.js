import React from 'react';

import {
    CSidebar,
    CSidebarBrand,
    CSidebarNav,
    CSidebarMinimizer,
    CSidebarNavItem,
    CImg,
    CCreateElement,
    CSidebarNavDivider,
    CSidebarNavDropdown,
    CSidebarNavTitle
} from '@coreui/react';

import Logo from '../../assets/images/logo-payme.svg';
import Signed from '../../assets/images/logo-sm.png';

import navigation from '../_nav';
import {useDispatch, useSelector} from "react-redux";

const TheSidebar = () => {
    const dispatch = useDispatch();
    const show = useSelector(state => state.sidebarShow);

    return (
        <CSidebar
            show={show} onShowChange={(val) => dispatch({type: 'set', sidebarShow: val})}>
            <CSidebarBrand className="d-md-down-none" to="/">
                <CImg
                    className="c-sidebar-brand-full"
                    src={Logo}
                    height={30}
                />
                <CImg
                    className="c-sidebar-brand-minimized"
                    src={Signed}
                    height={25}
                />
            </CSidebarBrand>
            <CSidebarNav>

                <CCreateElement
                    items={navigation}
                    components={{
                        CSidebarNavDivider,
                        CSidebarNavDropdown,
                        CSidebarNavItem,
                        CSidebarNavTitle
                    }}
                />
            </CSidebarNav>
            <CSidebarMinimizer className="c-d-md-down-none"/>
        </CSidebar>
    );
}

export default TheSidebar;
