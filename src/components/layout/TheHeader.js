import React from "react";
import {useSelector, useDispatch} from 'react-redux';

import {
    CHeader,
    CToggler,
    CHeaderBrand,
    CHeaderNav,
    CBreadcrumbRouter
} from '@coreui/react';

import {CIcon} from '@coreui/icons-react';

import {
    TheHeaderDropdownNotify,
    TheHeaderDropdown
} from './index';

import routes from '../../routers/routes';

const TheHeader = () => {
    const dispatch = useDispatch();
    const sidebarShow = useSelector(state => state.sidebarShow);

    const toggleSidebar = () => {
        const value = [true, 'responsive'].includes(sidebarShow) ? false : 'responsive';
        dispatch({type: 'set', sidebarShow: value});
    }

    const toggleSidebarMobile = () => {
        const value = [false, 'responsive'].includes(sidebarShow) ? true : 'responsive'
        dispatch({type: 'set', sidebarShow: value})
    }

    return (
        <CHeader withSubheader colorScheme="light">
            <CToggler
                inHeader
                className="ml-md-3 d-lg-none"
                onClick={toggleSidebarMobile}
            />
            <CToggler
                inHeader
                className="ml-3 d-md-down-none"
                onClick={toggleSidebar}
            />
            <CHeaderBrand className="mx-auto d-lg-none" to="/">
                <CIcon name={'logo'} height="48" alt="Logo"/>
            </CHeaderBrand>

            <CHeaderNav className="d-md-down-none mr-auto">
                <CBreadcrumbRouter
                    className="border-0 c-subheader-nav m-0 px-0 px-md-3"
                    routes={routes}
                />
            </CHeaderNav>

            <CHeaderNav className="ml-auto px-3">
                <TheHeaderDropdownNotify/>
                <TheHeaderDropdown/>
            </CHeaderNav>
        </CHeader>
    );
}

export default TheHeader;
