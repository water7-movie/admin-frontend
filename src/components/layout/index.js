import TheSidebar from "./TheSidebar";
import TheHeader from "./TheHeader";
import TheContent from "./TheContent";
import TheFooter from "./TheFooter";
import TheHeaderDropdownNotify from "./TheHeaderDropdownNotify";
import TheHeaderDropdown from "./TheHeaderDropdown";
import TheLayout from "./TheLayout";

export {
    TheSidebar,
    TheHeader,
    TheContent,
    TheFooter,
    TheHeaderDropdownNotify,
    TheHeaderDropdown,
    TheLayout
}
