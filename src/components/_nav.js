const nav = [
    {
        _tag: 'CSidebarNavItem',
        name: 'Dashboard',
        to: '/dashboard',
        icon: 'cil-speedometer'
    },
    // {
    //     _tag: 'CSidebarNavDropdown',
    //     name: 'Base',
    //     route: '/base',
    //     to: '/base',
    //     icon: 'cil-user',
    //     _children: [
    //         {
    //             _tag: 'CSidebarNavItem',
    //             name: 'Breadcrumb',
    //             to: '/base/breadcrumbs',
    //         }
    //     ]
    // },
    {
        _tag: 'CSidebarNavItem',
        name: 'Lecturers',
        route: '/lecturers',
        to: '/lecturers',
        icon: 'cil-user'
    },
];

export default nav;
