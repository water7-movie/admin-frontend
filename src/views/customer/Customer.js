import React from 'react';

import {
    CRow,
    CCol,
    CCard,
    CCardHeader,
    CCardBody, 
    CDropdown, 
    CDropdownToggle, 
    CDropdownMenu, 
    CDropdownItem
} from '@coreui/react';
import StripedTable from "../../components/tables/striped-table";

const Customer = () => {
    const items = [
        {id: 0, name: 'John Doe', regis: '2018/01/01', role: 'Guest', status: 'Pending'},
        {id: 1, name: 'Samppa Nori', regis: '2018/01/01', role: 'Member', status: 'Active'},
        {id: 2, name: 'Estavan Lykos', regis: '2018/02/01', role: 'Staff', status: 'Banned'},
        {id: 3, name: 'Chetan Mohamed', regis: '2018/02/01', role: 'Admin', status: 'Inactive'},
        {id: 3, name: 'Chetan Mohamed', regis: '2018/02/01', role: 'Admin', status: 'Inactive'},
        {id: 4, name: 'Derick Maximinus', regis: '2018/03/01', role: 'Member', status: 'Pending'},
        {id: 4, name: 'Derick Maximinus', regis: '2018/03/01', role: 'Member', status: 'Pending'},
        {id: 5, name: 'Friderik Dávid', regis: '2018/01/21', role: 'Staff', status: 'Active'},
        {id: 6, name: 'Yiorgos Avraamu', regis: '2018/01/01', role: 'Member', status: 'Active'},
        {id: 6, name: 'Yiorgos Avraamu', regis: '2018/01/01', role: 'Member', status: 'Active'},
        {id: 7, name: 'Avram Tarasios', regis: '2018/02/01', role: 'Staff', status: 'Banned'},
        {id: 7, name: 'Avram Tarasios', regis: '2018/02/01', role: 'Staff', status: 'Banned'},
        {id: 8, name: 'Quintin Ed', regis: '2018/02/01', role: 'Admin', status: 'Inactive'},
        {id: 9, name: 'Enéas Kwadwo', regis: '2018/03/01', role: 'Member', status: 'Pending'},
        {id: 10, name: 'Agapetus Tadeáš', regis: '2018/01/21', role: 'Staff', status: 'Active'}
    ];
    const fields = ['name', 'registered', 'role', 'status', 'actions'];
    const pagination = {
        pages: items.length / 10 + 1,
        align: 'end'
    };

    const paginationTest = (page) => {
        console.log(page);
    }

    return (
        <CRow>
            <CCol xs="12" sm="12" md="12" className="mb-4">
                <CCard borderColor="primary">
                    <CCardHeader>
                        <h3>Danh sách khách hàng</h3>
                    </CCardHeader>
                    <CCardBody>
                        <StripedTable
                            size={'sm'}
                            tableFilter={{label: 'Tìm kiếm', placeholder: 'Nội dung tìm'}}
                            hover={true}
                            fields={fields}
                            items={items}
                            sorter={true}
                            pagination={pagination}
                            onPageChange={paginationTest}
                            scopedSlots={{
                                'registered': (item) => (
                                    <td>{item.regis}</td>
                                ),
                                'actions': () => (
                                    <td>
                                        <CDropdown className="m-1 btn-group">
                                            <CDropdownToggle color="info">
                                                Thao tác
                                            </CDropdownToggle>
                                            <CDropdownMenu>
                                                <CDropdownItem>Thêm</CDropdownItem>
                                                <CDropdownItem>Cập nhật</CDropdownItem>
                                                <CDropdownItem>Xóa</CDropdownItem>
                                            </CDropdownMenu>
                                        </CDropdown>
                                    </td>
                                )
                            }}
                        />
                    </CCardBody>
                </CCard>
            </CCol>
        </CRow>
    );
}

export default Customer;


