import React, {useState} from 'react';

import {
    CRow,
    CCol,
    CCard,
    CCardBody,
    CTabs,
    CNav,
    CNavItem,
    CNavLink,
    CTabContent,
    CTabPane
} from '@coreui/react';

function Dashboard() {
    const [active, setActive] = useState(1)
    const lorem = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.'

    return (
        <>
            <CRow>
                <CCol xs="12" sm="12" md="12" className="mb-4">
                    <CCard borderColor="primary">
                        <CCardBody>
                            <CTabs activeTab={active} onActiveTabChange={idx => setActive(idx)}>
                                <CNav variant="tabs">
                                    <CNavItem>
                                        <CNavLink>Giao dịch 1</CNavLink>
                                    </CNavItem>
                                    <CNavItem>
                                        <CNavLink>Tài khoản</CNavLink>
                                    </CNavItem>
                                    <CNavItem>
                                        <CNavLink>Dịch vụ</CNavLink>
                                    </CNavItem>
                                </CNav>
                                <CTabContent>
                                    <CTabPane>
                                        {`1. ${lorem}`}
                                    </CTabPane>
                                    <CTabPane>
                                        {`2. ${lorem}`}
                                    </CTabPane>
                                    <CTabPane>
                                        {`3. ${lorem}`}
                                    </CTabPane>
                                </CTabContent>
                            </CTabs>
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>

        </>
    );
}

export default Dashboard;


