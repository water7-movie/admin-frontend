import React from 'react'

import {
    CContainer,
    CRow,
    CCol,
    CCardGroup,
    CCard,
    CCardBody,
    CForm,
    CInputGroup,
    CInputGroupPrepend,
    CInputGroupText,
    CInput,
    CButton
} from "@coreui/react";

import CIcon from '@coreui/icons-react';

function Login() {
    return (
        <div className="c-app flex-row align-items-center">
            <CContainer>
                <CRow className="justify-content-center">
                    <CCol xs="4">
                        <CCardGroup>
                            <CCard className="p-4">
                                <CCardBody>
                                    <CForm>
                                        <h1 className="card-title text-center">LOGIN</h1>
                                        <p className="card-subtitle mb-2 text-muted">Sign In to your account</p>
                                        <CInputGroup className="mb-3">
                                            <CInputGroupPrepend>
                                                <CInputGroupText>
                                                    <CIcon name="cil-user"/>
                                                </CInputGroupText>
                                            </CInputGroupPrepend>
                                            <CInput type="text" placeholder="Username" autoComplete="username"/>
                                        </CInputGroup>
                                        <CInputGroup className="mb-4">
                                            <CInputGroupPrepend>
                                                <CInputGroupText>
                                                    <CIcon name="cil-lock-locked"/>
                                                </CInputGroupText>
                                            </CInputGroupPrepend>
                                            <CInput type="password" placeholder="Password"
                                                    autoComplete="current-password"/>
                                        </CInputGroup>
                                        <CRow className="text-right">
                                            <CCol>
                                                <CButton color="info" className="px-4">Login</CButton>
                                            </CCol>
                                        </CRow>
                                    </CForm>
                                </CCardBody>
                            </CCard>
                        </CCardGroup>
                    </CCol>
                </CRow>
            </CContainer>
        </div>
    );
}

export default Login;
