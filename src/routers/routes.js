import Dashboard from "../views/dashboard";
import Customer from "../views/customer";

const routes = [
    {
        path:'/dashboard',
        exact: true,
        name: 'Dashboard',
        component: Dashboard
    },
    {
        path:'/lecturers',
        exact: true,
        name: 'Lecturers',
        component: Customer
    }
];

export default routes;
