import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import './assets/scss/style.scss';
import Login from "./views/login";
import TheLayout from "./components/layout/TheLayout";
import Page404 from "./views/errors/page404";
import Page500 from "./views/errors/page500";

function App() {
    return (
        <div>
            <BrowserRouter>
                <Switch>
                    <Route exact path="/login" name="Login Page" render={props => <Login {...props}/>}/>
                    <Route exact path="/404" name="Page 404" render={props => <Page404 {...props}/>} />
                    <Route exact path="/500" name="Page 500" render={props => <Page500 {...props}/>} />
                    <Route path="/" name="Home" render={props => <TheLayout {...props}/>}/>
                </Switch>
            </BrowserRouter>
        </div>
    );
}

export default App;
